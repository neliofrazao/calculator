import '../style/main.scss';

import { ReceiptsForm } from './components/ReceiptsForm/ReceiptsForm';

{
  const receiptsForm = new ReceiptsForm();
  receiptsForm.init();
}
