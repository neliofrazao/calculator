export const errorMessages = {
  ERROR_TIMEOUT: 'Infelizmente estamos com problemas de conexão, tente novamente mais tarde.',
  ERROR_DEFAULT: 'Ops! Sistema instável, tente novamente mais tarde!',
  NETWORK_ERROR: 'Para utilizar o nosso sistema é necessário estar conectado a uma Rede',
};
